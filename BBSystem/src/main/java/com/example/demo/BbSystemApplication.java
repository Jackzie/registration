package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BbSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(BbSystemApplication.class, args);
	}

}
