package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Reg;
import com.example.demo.service.RegService;

@RestController
@CrossOrigin(origins ="http://localhost:4200")
public class RegController {
	@Autowired
	RegService service;
	@PostMapping("/reg")
	public Reg regUser(@RequestBody Reg reg){
        Reg regobj=null;
		regobj=service.saveUser(reg);
		return regobj;
	}

}
