package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Reg;
import com.example.demo.repository.regRepository;

@Service
public class RegService {
	@Autowired
	regRepository repo;
	public Reg saveUser(Reg reg) {
		return repo.save(reg);
		
	}

}
