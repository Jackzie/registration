package com.example.demo.controller;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.example.demo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.model.Vendor;
import com.example.demo.repository.AdminRepository;
import com.example.demo.service.Service_Impl;
@Controller
@RestController
@CrossOrigin(origins="http://localhost:4300")
@RequestMapping("api/v1")
public class AdminController {
	
		@Autowired
		private Service_Impl serviceimpl;
		@Autowired
		private AdminRepository adminrepository;
		
		@GetMapping("/vendors")
		public ResponseEntity<List<Vendor>> getAll() {
			return new ResponseEntity<>(serviceimpl.getServAllVendors(),HttpStatus.OK);
		}
		@GetMapping("/vendors/status/{status}")
		public ResponseEntity<List<Vendor>> getVendorByStatus(@PathVariable String status){
			return new ResponseEntity<>(serviceimpl.getVendorServ(status),HttpStatus.OK);
		}
		@PutMapping("/vendors/approve/{id}")
		public ResponseEntity<Vendor> updateVendor(@PathVariable int id,@RequestBody Vendor vendor)
		{	 Vendor updatedVendor = adminrepository.findById(id).orElseThrow(()->
		new NullPointerException());		
		updatedVendor.setStatus(vendor.getStatus());		 
		String result;
		        Properties props = System.getProperties();
		        String host = "smtp.gmail.com";
		        String from = "//sender mail address";
		        String pass = "//sender mail password";
		        String to = updatedVendor.getEmail();
		        props.put("mail.smtp.starttls.enable", "true");
		        props.put("mail.smtp.host", host);
		        props.put("mail.smtp.user", from);
		        props.put("mail.smtp.password", pass);
		        props.put("mail.smtp.port", "587");     //try 465, 25, 587
		        props.put("mail.smtp.auth", "true");
		        props.put("mail.smtp.timeout", "25000");		   
		        Session mailsession = Session.getDefaultInstance(props);
		       
		   try{		     
		        MimeMessage message = new MimeMessage(mailsession);		      
		        message.setFrom(new InternetAddress(from));  
		        message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));		       
		        message.setSubject("Welcome to BlueBerry Travels");	       
		        message.setText("Successfully your registration has been approved by the admin");			       
		        Transport transport = mailsession.getTransport("smtp");
		        transport.connect(host, from, pass);
		        transport.sendMessage(message, message.getAllRecipients());
		        transport.close();
		        result = "Sent message successfully....";
		    }catch (MessagingException mex) {
		      mex.printStackTrace();
		      result = "Error: unable to send message....";
		    }
	return ResponseEntity.ok(adminrepository.save(updatedVendor));
		}
		@PutMapping("/vendors/reject/{id}")
		public ResponseEntity<Vendor> updateVendor2(@PathVariable int id,@RequestBody Vendor vendor)
		{	 Vendor updatedVendor2 = adminrepository.findById(id).orElseThrow(()->
		new NullPointerException());		
		updatedVendor2.setStatus(vendor.getStatus());		 
		String result;
		        Properties props = System.getProperties();
		        String host = "smtp.gmail.com";
		         String from = "//sender mail address";
		        String pass = "//sender mail password";
		        String to = updatedVendor2.getEmail();
		        props.put("mail.smtp.starttls.enable", "true");
		        props.put("mail.smtp.host", host);
		        props.put("mail.smtp.user", from);
		        props.put("mail.smtp.password", pass);
		        props.put("mail.smtp.port", "587");     //try 465, 25, 587
		        props.put("mail.smtp.auth", "true");
		        props.put("mail.smtp.timeout", "25000");		   
		        Session mailsession = Session.getDefaultInstance(props);
		   try{		     
		        MimeMessage message = new MimeMessage(mailsession);		      
		        message.setFrom(new InternetAddress(from));  
		        message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));		       
		        message.setSubject("BetterLuck next time");	       
		        message.setText("Unfortunately your travel with BB travels has been terminated");			       		        	
		        Transport transport = mailsession.getTransport("smtp");
		        transport.connect(host, from, pass);
		        transport.sendMessage(message, message.getAllRecipients());
		        transport.close();
		        result = "Sent message successfully....";
		    }catch (MessagingException mex) {
		      mex.printStackTrace();
		      result = "Error: unable to send message....";
		    }
		   return ResponseEntity.ok(adminrepository.save(updatedVendor2));
		   }	
	}
		
	

