package com.example.demo.controller;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.BookingsModel;
import com.example.demo.repository.BookingsRepository;

@RestController
@CrossOrigin(origins="http://localhost:4300")
@RequestMapping("api/v2")
public class BookingsController {

	@Autowired
	private BookingsRepository bookingsrepository;
	@GetMapping("/bookings")
		public ResponseEntity<List<BookingsModel>> getAll() {
			return ResponseEntity.ok().body(bookingsrepository.findAll());
		}
	
		
	}
		
	

