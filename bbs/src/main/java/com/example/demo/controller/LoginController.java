package com.example.demo.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.repository.*;
import com.example.demo.service.LoginService;
import com.example.demo.model.*;

@RestController
@CrossOrigin(origins = "http://localhost:4300")

public class LoginController {
	@Autowired
	private LoginService service;
	
	
	
	@PostMapping("/login1")
	public Login login(@RequestBody Login lgd) throws Exception{
		String username=lgd.getUsername();
		String password=lgd.getPassword();
		
		System.out.println(username);
		System.out.println(password);
		
	//	HttpSession session = null;
		
		Login loginobj=null;
		if(username !=null && password !=null) {
			
			loginobj=service.findByUsernameAndPassword(username, password);
			
		}
		if(loginobj==null)
		{
			throw new Exception("Bad credentials");
		}
		return loginobj;
		
	}
	

}