package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.ReviewsRatingsModel;
import com.example.demo.repository.ReviewsRatings;

@RestController
@CrossOrigin(origins="http://localhost:4300")
@RequestMapping("api/v3")
public class ReviewsRatingsController {

	@Autowired
	private ReviewsRatings reviewsratings;
	@GetMapping("/reviews")
		public ResponseEntity<List<ReviewsRatingsModel>> getAll() {
			return ResponseEntity.ok().body(reviewsratings.findAll());
		}
	@DeleteMapping("/reviews/delete/{id}")
	public void deletereviews(@PathVariable int id){
		
		reviewsratings.deleteById(id);
		
		
	}
}	
	

