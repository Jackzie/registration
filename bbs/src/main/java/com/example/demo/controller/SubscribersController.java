package com.example.demo.controller;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.model.SubscribersModel;
import com.example.demo.repository.SubscribersRepository;
@RestController
@CrossOrigin(origins="http://localhost:4300")
@RequestMapping("api/v4")
public class SubscribersController {
	@Autowired
	private SubscribersRepository subrepository;
	@GetMapping("/subscribers")
		public ResponseEntity<List<SubscribersModel>> getAll() {
			return ResponseEntity.ok().body(subrepository.findAll());
		}
	
}
