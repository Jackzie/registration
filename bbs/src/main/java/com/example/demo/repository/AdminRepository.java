package com.example.demo.repository;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;


import com.example.demo.model.Vendor;
@Repository("adminRepo")
public interface AdminRepository extends JpaRepository<Vendor, Integer> {
	
	@Query("select a from approval_List a where status=?1 ")
	public List<Vendor> getVendorsByStatus(String status);

	

	
}
