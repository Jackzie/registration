package com.example.demo.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.SubscribersModel;
public interface SubscribersRepository extends JpaRepository<SubscribersModel, Integer>{

}
