package com.example.demo.service;

import java.util.List;

import com.example.demo.model.Vendor;

public interface Service {
	public List<Vendor> getServAllVendors();
	public List<Vendor> getVendorServ(String status);	
	
}
