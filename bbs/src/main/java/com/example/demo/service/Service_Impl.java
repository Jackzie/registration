package com.example.demo.service;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;
import com.example.demo.model.Vendor;
import com.example.demo.repository.AdminRepository;
@Service("serviceimpl")
@Transactional
public class Service_Impl  implements Service{
	@Autowired
	private AdminRepository repo;
	
	public List<Vendor> getServAllVendors(){
		return repo.findAll();
	}
	public List<Vendor> getVendorServ(String status){
		return repo.getVendorsByStatus(status);
	}
	
	
	@Override
	public Class<? extends Annotation> annotationType() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String value() {
		// TODO Auto-generated method stub
		return null;
	}
}
