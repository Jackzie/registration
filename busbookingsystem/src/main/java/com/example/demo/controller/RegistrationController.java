package com.example.demo.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.model.AddDetails;
import com.example.demo.service.RegistrationService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class RegistrationController {
	@Autowired
	private RegistrationService service1;
	@PostMapping("/reg")
	public AddDetails reg(@RequestParam("file") MultipartFile file, @RequestParam("user") String user) throws JsonMappingException,JsonProcessingException, IOException {
		AddDetails reg= new ObjectMapper().readValue(user, AddDetails.class);
		reg.setProof(file.getBytes());
		AddDetails dbreg=service1.saveuser(reg);
		return dbreg;
		}
	
	@GetMapping("/getdetails")
	public List<AddDetails>  getAllDetails()
	{
		return service1.getAllDetails();
		
	}
	
}
