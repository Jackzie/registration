package com.example.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="addbus1_details")
public class AddDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private  String busname;
	private String busnum;
	private String bustype;
	private String noseats;
	private String permitdetails;
	private byte [] proof;
	public byte[] getProof() {
		return proof;
	}
	public void setProof(byte[] proof) {
		this.proof = proof;
	}
	public String getBusname() {
		return busname;
	}
	public void setBusname(String busname) {
		this.busname = busname;
	}
	public String getBusnum() {
		return busnum;
	}
	public void setBusnum(String busnum) {
		this.busnum = busnum;
	}
	public String getBustype() {
		return bustype;
	}
	public void setBustype(String bustype) {
		this.bustype = bustype;
	}
	public String getNoseats() {
		return noseats;
	}
	public void setNoseats(String noseats) {
		this.noseats = noseats;
	}
	public String getPermitdetails() {
		return permitdetails;
	}
	public void setPermitdetails(String permitdetails) {
		this.permitdetails = permitdetails;
	}
	
	

}
