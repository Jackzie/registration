package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.AddDetails;

public interface RegRepository extends JpaRepository<AddDetails, Long>{

}
