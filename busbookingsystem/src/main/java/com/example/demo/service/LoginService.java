package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Login;
import com.example.demo.repository.LoginRepository;

@Service
public class LoginService {
	@Autowired
	private LoginRepository repo;
	public Login findByUsernameAndPassword(String username,String password) {
		return repo.findByUsernameAndPassword(username, password);
	}
	

}
