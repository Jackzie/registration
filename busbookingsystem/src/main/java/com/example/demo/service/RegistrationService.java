package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.AddDetails;
import com.example.demo.repository.RegRepository;
@Service
public class RegistrationService {
	@Autowired
	RegRepository repo;
	public AddDetails saveuser(AddDetails reg)
	{
		
		return repo.save(reg);
		
	}
	 public List<AddDetails> getAllDetails()
	 {
		 return repo.findAll();
	 }
	
	
}
